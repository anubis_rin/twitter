Encoding.default_external = 'UTF-8'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/static_assets'
require 'digest/md5'
require_relative 'models/comment_db'
require_relative 'models/user_db'

enable :sessions

helpers do
  include Rack::Utils
  alias_method :h, :escape_html
  
  #指定されたユーザーのGravatarを表示
  def gravatar_img(user)
    gravatar_id = Digest::MD5::hexdigest(user.address.downcase)
    gravatar_url = "https:secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url)
  end
  
  #brタグ変換
  def convertBr(str)
    str.gsub(/\r\n|\r|\n/,'<br />')
  end
end 

get '/' do
  @user = User.where(_id: session[:user_id]).first
  if @user
    @comments = Comment.userTweet(@user.userID)
    @tweetCount = Comment.tweetCount(@user.userID)
    erb :index
  else
    redirect '/login'
  end
end

get '/signup' do
  erb :signup
end

post '/signup' do
  if params[:password] != params[:password_conf]
    redirect'/signup'
  end
  
  if User.signup(params[:username], params[:userid], params[:address], params[:password])
    redirect '/login'
  else
    redirect '/signup'
  end
end

get '/login' do
  if session[:user_id]
    redirect '/logout'
  end
  
  erb :login
end

post '/login' do
  user = User.auth(params[:userid], params[:password])
  if user
    session[:user_id] = user._id
    redirect '/'
  else
    redirect '/login'
  end
end

get '/logout' do
  unless session[:user_id]
    redirect '/login'
  end
  erb :logout
end

delete '/logout' do
  session[:user_id] = nil
  redirect '/login'
end

post '/posting' do
  postTime = Time.now.strftime('%Y-%m-%d %H:%M:%S')
  Comment.insert(params[:userName],params[:userID],params[:comment],postTime)
  redirect '/'
end

get '/profile' do
  @user = User.where(_id: session[:user_id]).first
  if @user
    erb :profile
  else
    redirect '/login'
  end
end

post '/profile' do
  User.update(params[:userID], params[:userName], params[:profile])
  redirect '/profile'
end

post '/search' do
  @keyword = params[:keyword]
  @search_comments = Comment.search(params[:keyword])
  erb :result
end

delete '/delete' do
  Comment.delete(params[:id])
  redirect '/'
end

get '/:userid' do
  @userinfo = User.where(userID: params[:userid]).first
  @usercomments = Comment.where(userID: params[:userid])
  if @userinfo && @usercomments
    erb :userpage
  end
end