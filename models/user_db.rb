Encoding.default_external = 'UTF-8'
require 'mongoid'
require 'digest/sha1'
Mongoid.load!('./mongoid.yml')

class User
  include Mongoid::Document
  field :userName,type:String
  field :userID,type:String
  field :address,type:String
  field :profile,type:String
  field :password_salt,type:String
  field :password_hash,type:String
  
  attr_readonly :password_salt, :password_hash
  validates_presence_of :userName
  validates_presence_of :userID
  validates_uniqueness_of :userID
  validates_presence_of :address
  validates_uniqueness_of :address
  validates_format_of :address, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates_presence_of :password_salt
  validates_presence_of :password_hash
  
  #ユーザー作成
  def self.signup(userName, userid, address, password)
    user = User.new
    user.userName = userName
    user.userID = userid
    user.address = address
    user.password_salt = self.salt(userid)
    user.password_hash = self.hash(password, user.password_salt)
    user.save
  end
  
  #ユーザー情報更新
  def self.update(userID, userName, profile)
    user = self.where(userID: userID).first
    user.userName = userName
    user.profile = profile
    user.save
  end
  
  #ユーザー認証
  def self.auth(userid, password)
    user = self.where(userID: userid).first
    if user && user.password_hash == self.hash(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  #salt生成
  def self.salt(userid)
    Digest::SHA1.hexdigest("#{Time.now.to_s}#{userid}")
  end
  
  #ハッシュ化
  def self.hash(password, salt)
    Digest::SHA1.hexdigest("#{salt}#{password}")
  end
end
