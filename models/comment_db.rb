Encoding.default_external = 'UTF-8'
require 'mongoid'
Mongoid.load!('./mongoid.yml')

class Comment
  include Mongoid::Document
  field :userName,type:String
  field :userID,type:String
  field :postTime,type:String
  field :comment,type:String

  #コメント表に登録
  def self.insert(userName,userID,comment,postTime)
    comments = Comment.new
    comments.userName = userName
    comments.userID = userID
    comments.comment = comment
    comments.postTime = postTime
    comments.save
  end
  #コメント表から全件取得
  def self.search_all
    comments = Comment.all.desc(:postTime)
    return comments
  end
  
  #入力されたキーワードからコメントを検索
  def self.search(keyword)
    comments = Comment.where(comment: /#{keyword}/).desc(:postTime)
    return comments
  end
  
  #指定されたコメントを削除
  def self.delete(id)
    comment = Comment.where(_id: "#{id}").first
    comment.delete
  end
  
  #指定されたユーザーのツイート数
  def self.tweetCount(userID)
    Comment.where(userID: "#{userID}").count
  end
  
  #指定されたユーザーのツイートを全て取得
  def self.userTweet(userID)
    Comment.where(userID: "#{userID}")
  end
end
