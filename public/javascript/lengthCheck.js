
$(document).ready(function(){
	var textLength = {"maxSize" :140 };
	Object.freeze(textLength);
	var message = document.getElementById("message");
	var button = document.getElementById("post_button");
	var text = document.getElementById("post_text");
	text.addEventListener("keyup",function(){
		length = text.value.length;
		if(length > textLength.maxSize){
			button.disabled = "true";
			message.innerHTML = "文字数オーバー";
		}else{
			button.disabled = "";
			message.innerHTML = "";
		}
	});
});

